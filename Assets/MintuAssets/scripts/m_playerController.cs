using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class m_playerController : MonoBehaviour
{
    private CharacterController m_charactercontroller;
    private Vector3 _move_direction;
    private float _speed = 3.5f;
    private float _gravity = 9.81f;

    // Start is called before the first frame update
    void Start()
    {
        m_charactercontroller = GetComponent<CharacterController>();
        
    }

    // Update is called once per frame
    void Update()
    {
        player_movement();
    }

    public void player_movement()
    {
        float _horizontalInput = Input.GetAxis("Horizontal");
        float _verticalInput = Input.GetAxis("Vertical");

        _move_direction = new Vector3(_horizontalInput,0f,_verticalInput);
        Vector3 velocity = _move_direction * _speed;
        velocity.y -= _gravity;

        m_charactercontroller.Move(velocity * Time.deltaTime);
        Debug.Log(_move_direction);
    }
}
